<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Services\Menu\MenuService;
use App\Http\Services\Product\User\ProductUserService;
use App\Http\Services\Slider\SliderService;
use Illuminate\Http\Request;

class ClientMainController extends Controller
{

    protected $menuService;
    protected $sliderService;
    protected $productUserService;

    /**
     * @param $menuService
     */
    public function __construct(SliderService $sliderService, MenuService $menuService, ProductUserService $productUserService)
    {
        $this->sliderService = $sliderService;
        $this->menuService = $menuService;
        $this->productUserService = $productUserService;
    }


    public function index()
    {
        return view('user.main', [
            'title' => 'Shop Quần Áo',
            'sliders' => $this->sliderService->show(),
            'menus' => $this->menuService->show(),
            'products' => $this->productUserService->get()
        ]);
    }

    public function loadProduct(Request $request)
    {
        $page = $request->input('page', 0);

        $result = $this->productUserService->get($page);

        if (count($result) != 0) {
            $html = view('user.products.list', ['products' => $result ])->render();

            return response()->json(['html' => $html]);
        }
        return response()->json(['html' => '']);
    }
}
